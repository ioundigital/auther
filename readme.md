# Auther
## A Laravel 5.6+ authentification package made for developers

### Features
- Role based authentication middleware
- Helper facade to help with handling users
- Easy to use CLI commands for adding roles and users
- Easily extensible User class with a default relationship to hook into your own Profile class

### Installation

With your Laravel instance (5.6+) set up and connected to a database, run the following

```
composer require ioundigital/auther
php artisan vendor:publish
php artisan migrate
```

To set up your first user, run `php artisan auther:newuser` and follow the prompts

### Middleware

Auther provides an easy to use Middleware filter that allows you to filter for logged in users, and optionally by user role. Users who fail authentication will be redirected to the named route you specify in the `auther.php` config file.

```
// to restrict routes so that ANY user can access them
Route::middleware('auther')->group(function () {
    Route::get('profile', 'Controller@profile');
});

// to restrict routes so that only 'admin' type users can access them
Route::middleware('auther:admin')->group(function () {
    Route::get('dashboard', 'Controller@dashboard');
});
```

### Auther Facade

Rather than providing log-in, registration, or password reset views and routes out of the box, we've left that in your capable hands. Instead, you can interact with these valuable parts of the user system through the Auther facade.

#### Log in / Log out

The three main pieces of the Auther facade are involved in the login process. First you authenticate the request, log in the user, and then log them out.

##### authenticate()

To verify a login request, pass the user's handle and plain-text password to this function. Returns true on success, false on failure
```
$handle = $input[ 'handle' ];
$passwd = $input[ 'passwd' ];

if( Auther::authenticate( $handle, $passwd ) ) {
    // do something with the valid user
}
```

##### login()

To log in a user, pass their handle to this function. Following the example above, the ideal login pattern would be something like this:
```
$handle = $input[ 'handle' ];
$passwd = $input[ 'passwd' ];

if( Auther::authenticate( $handle, $passwd ) ) {
    Auther::login( $handle );
} else {
    // do something with a bad login request
}
```

##### logout()

To log out the current user, simply call the logout function
```
Auther::logout();
```

#### Password Reset

Auther provides a key based password reset functionality. Keys track both the IP and timestamp from when they are issued.


##### request_key()

To request a reset key, pass the user object to this function. Idealy you would email this key to your user.

```
$user = Auther::user( $handle );
$key = Auther::request_key( $user );
```

##### test_key()

To test a reset key, pass it to this function. if the key is valid and matches the user you pass in, it will have its used_by date and IP set and will no longer be valid.
```
$user = Auther::user( $handle );
$key = $input['key'];
if( Auther::test_key( $user, $key ) ) {
    // allow the user to reset their password
}
```

#### password()

To get an encrypted password for the user to update to, use the password function.
```
$user->password = Auther::password( 'someplaintext' );
$user->save();
```

##### user()

By default, this function returns the current logged in user. However, to get a specific user by their ID, you can pass that in.
```
// to get the current logged in user
$user = Auther::user();

// to get a user with the ID 5
$user = Auther::user( 5 );
```

##### role()

By default this returns the current logged in users role. However, to get a specific role, pass in that role's slug
```
// current logged in user role
$role = Auther::role();

// get the role called 'subscriber'
$role = Auther::role( 'subscriber' );
```

### Todo
- alias the middleware so that it can be used without the fully qualified class name