<?php

namespace Ioundigital\Auther;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Ioundigital\Auther\Commands\NewUserCommand;
use Ioundigital\Auther\Commands\NewRoleCommand;

class AutherServiceProvider extends ServiceProvider {
	
	public function boot()
	{
		Schema::defaultStringLength(191);
		$this->loadMigrationsFrom(__DIR__.'/resources/migrations');
		$this->loadRoutesFrom(__DIR__.'/resources/routes/web.php');
		$this->loadViewsFrom(__DIR__.'/resources/views','auther');
		
		$router = $this->app['router'];
		$router->aliasMiddleware('auther',AutherMiddleware::class);
		
		$this->publishes([
			__DIR__.'/resources/config/auther.php' => config_path('auther.php')
		]);
		
		if( $this->app->runningInConsole()) {
			$this->commands([
				NewUserCommand::class,
				NewRoleCommand::class
			]);
		}
	}

	public function register() {
		$this->app->singleton('auther', function($app) {
			return new AutherService();
		});
		
		$this->mergeConfigFrom(
			__DIR__.'/resources/config/auther.php', 'auther'
		);
	}

	public function providers() {
		return ['auther'];
	}
}
