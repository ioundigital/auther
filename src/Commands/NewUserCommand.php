<?php

namespace Ioundigital\Auther\Commands;

use Ioundigital\Auther\Models\User;
use Ioundigital\Auther\Models\Role;
use Illuminate\Console\Command;
use Hash;

class NewUserCommand extends Command {
    
    protected $signature = 'auther:newuser';
    
    protected $description = 'Create a new Auther user';
    
    public function handle()
    {
        $this->info('Welcome to the Auther user wizard');
        $this->testForAdminRole();
        
        $user = new User();
        $user->handle = $this->ask('User Handle?');
        $user->email = $this->ask('User email?');
        
        $password = $this->getPassword();
        $user->password = $password;
        
        $role = $this->ask('User role slug?');
        $role = Role::where('slug', $role )->first();
        $user->role()->associate($role);
        
        if( $this->confirm("Create new user " . $user->handle . " ( " . $user->email . " ) of role " . $role->slug . "?" ) ) {
            $user->save();
            $this->info("Done!");
        }
        
    }
    
    public function getPassword()
    {
        $pass1 = $this->secret('User password?');
        $pass2 = $this->secret('Confirm password');
        
        if( $pass1 != $pass2 ) {
            $this->error("passwords did not match!" );
            return $this->getPassword();
        }
        
        return Hash::make( $pass1 );
    }
    
    public function testForAdminRole()
    {
        if( Role::where('slug','admin')->count() != 0 ) {
            return;
        }
        
        if( $this->confirm("No admin role was found. Would you like to set one up now?") ) {
            if( $this->confirm("Use default role name 'Admin' with slug 'admin'?") ) {
                $role = new Role();
                $role->name = 'Admin';
                $role->slug = 'admin';
                $role->save();
                $this->info('created admin role!');
            } else {
                $slug = $this->ask('What is the slug?');
                $name = $this->ask('What is the name?');
                
                if( $this->confirm('Create new role ' . $name . ' with slug: ' . $slug . '?' ) ) {
                    $role = new Role();
                    $role->slug = $slug;
                    $role->name = $name;
                    $role->save();
                    $this->info('Role created!');
                }
            }
        } else {
            return;
        }
    }
}