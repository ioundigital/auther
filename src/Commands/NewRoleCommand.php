<?php

namespace Ioundigital\Auther\Commands;

use Ioundigital\Auther\Models\Role;
use Illuminate\Console\Command;

class NewRoleCommand extends Command {
    
    protected $signature = 'auther:newrole';
    
    protected $description = 'Create a new Auther role';
    
    public function handle()
    {
        $slug = $this->ask('What is the slug?');
        $name = $this->ask('What is the name?');
        
        if( $this->confirm('Create new role ' . $name . ' with slug: ' . $slug . '?' ) ) {
            $role = new Role();
            $role->slug = $slug;
            $role->name = $name;
            $role->save();
            $this->info('Role created!');
        }
    }
}