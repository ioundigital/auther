<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserScheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function( Blueprint $t ) {
            $t->increments('id');
            $t->string('slug');
            $t->string('name');
            
            $t->timestamps();
            $t->softDeletes();
        });
        
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('handle');
            $table->string('email')->unique();
            $table->string('password');
            
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::create('keys', function( Blueprint $t ) {
            $t->increments('id');
            $t->string('value');
            $t->dateTime('expires');
            
            $t->integer('user_id')->unsigned();
            $t->foreign('user_id')->references('id')->on('users');
            
            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
