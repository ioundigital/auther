<?php

namespace Ioundigital\Auther;

use Illuminate\Support\Facades\Facade;

class AutherFacade extends Facade {

	protected static function getFacadeAccessor() {
		return 'auther';
	}
}
