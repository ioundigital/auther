<?php

namespace Ioundigital\Auther\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    public function role()
    {
        return $this->belongsTo('Ioundigital\Auther\Models\Role');
    }
    
    public function key()
    {
        return $this->hasOne('Ioundigital\Auther\Models\Key');
    }
    
    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }
}