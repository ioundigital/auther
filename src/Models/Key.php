<?php

namespace Ioundigital\Auther\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Key extends Model {
    
    use SoftDeletes;
    
    public function user()
    {
        return $this->belongsTo('Ioundigital\Auther\Models\User');
    }
    
}