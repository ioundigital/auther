<?php

namespace Ioundigital\Auther;

use Session;
use Ioundigital\Auther\Models\User;
use Ioundigital\Auther\Models\Role;
use Ioundigital\Auther\Models\Key;

class AutherService {
	
	/**
	 * Static functions
	 */
	public static function user( $id = null )
	{
		$user = null;
		if( $id != null ) {
			$user = User::find($id);
		} else {
			if( Session::has( '_uid' ) ) {
				$user = User::find( Session::get( '_uid' ) );
			}
		}
		return $user;
	}

	public static function role( $slug = null )
	{
		$role = null;
		if( $slug != null ) {
			$role = Role::where('slug', $slug )->first();
		} else {
			if( Session::has( '_uid' ) ) {
				$role = this::user()->role;
			}
		}
		return $role;
	}

	public static function key( $value = null )
	{
		$key = null;
		if( $value != null ) {
			$key = Key::where( 'value', $value )->first();
		} else {
			if( Session::has( '_uid' ) ) {
				$key = Key::where( 'user', this::user() )->first();
			}
		}
		return $key;
	}

	public static function is( $role )
	{
		$user = this::user();
		if( $user == null ) return false;
		return $user->role->slug == $role;
	}

	public static function password( $raw )
	{
		return Hash::make( $raw );
	}
	
	/**
	 * Actions
	 */
	public function authenticate( $handle, $password )
	{
		$user = User::where( 'handle', $handle )->first();
		if( empty( $user ) ) return false;
		return Hash::check( $password, $user->password );
	}

	public function login( $handle )
	{
		$user = User::where( 'handle', $handle )->first();
		if( empty( $user ) ) return false;
		Session::put( '_uid', $user->id );
	}

	public function logout()
	{
		Session::forget( '_uid' );
	}

	public function request_key( $user )
	{
		$key = new Key;
		$key->value = Hash::make( date() );
		$key->issued_at = date();
		$key->issued_from_ip = $_SERVER['REMOTE_ADDR'];
		$key->user->associate( $user );
		$key->save();

		return $key->value;
	}

	public function test_key( $user, $key )
	{
		$match = false;
		if( $user->key->value == $key->value ) {
			$match = true;
			$key->used_at = date();
			$key->used_from_ip = $_SERVER['REMOTE_ADDR'];
			$key->save();
		}

		return $match;
	}

}
