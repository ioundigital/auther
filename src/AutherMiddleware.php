<?php

namespace Ioundigital\Auther;

use Closure;
use Session;
use Ioundigital\Auther\Models\User;

class AutherMiddleware {
    
    public function handle( $request, Closure $next, $role = null )
    {
        // set where to send them if they're not authenticated
        $default = config('auther.defaultroute');
        $verified = false;
        
        // ensure there is a user
        if( Session::has('_uid') ) {
            if( $role == null ) {
                // there's no role test so just approve it
                $verified = true;
            } else {
                //time to find out if the user can do that
                $user = User::find( Session::get('_uid'));
                if( !empty( $user ) ) {
                    $user_role = $user->role->slug;
                    if( $user_role == $role ) {
                        $verified = true;
                    }
                }
                
            }
        }
        
        
        if( $verified ) {
            return $next($request);
        } else {
            return redirect()->route( $default );
        }
        
    }
}